package esprit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import esprit.entities.GpsCoordinate;
@Repository
public interface GpsCoordinateRepository extends JpaRepository<GpsCoordinate, Long> {

}
