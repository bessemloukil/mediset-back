package esprit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import esprit.entities.Pharmacie;
@Repository
public interface PharmacieRepository extends JpaRepository<Pharmacie, Long> {

}
