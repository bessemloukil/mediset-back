package esprit.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import esprit.entities.*;


@Repository
public interface UserRepository  extends JpaRepository<User, Long> {
	public User findByEmail( String email);
	public User AddAdresse( GpsCoordinate adresse_id ,String user_name);
}
