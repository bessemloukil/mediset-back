package esprit.dao;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import esprit.entities.Report;
@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
	
	public List<Report> getReportsByPharmacie(int pharmacie_id);
	
}
