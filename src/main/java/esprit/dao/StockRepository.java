package esprit.dao;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import esprit.entities.Stock;
@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {
	public List<Stock> findByPharmacie( Long pharmacie_id);
	public List<Stock> filterPharmacieByMedicament( Long medicament_id);
}
