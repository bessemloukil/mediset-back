package esprit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import esprit.entities.Client;
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

}
