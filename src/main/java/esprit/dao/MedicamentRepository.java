package esprit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import esprit.entities.Medicament;
@Repository
public interface MedicamentRepository extends JpaRepository<Medicament, Long> {

}
