package esprit.services;

import java.util.List;
import esprit.entities.Report;

public interface ReportService {
	public List<Report> getReportsByPharmacie(int id);
	public Boolean addReport(int pharmacie_id, int client_id, int medicament_id);
	
}
