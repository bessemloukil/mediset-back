package esprit.services;

public interface Factory<T,Y> {
	T create(Y input);

}
