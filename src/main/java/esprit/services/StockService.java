package esprit.services;

import java.util.List;
import java.util.Optional;

import esprit.entities.Medicament;
import esprit.entities.Pharmacie;
import esprit.entities.Stock;

public interface StockService {
	public List<Stock> getStocks(Long id);
	public List<Stock> addStock(Long id_ph, float nb_stock,Medicament med);
	public boolean updateStock(Long id_stock,float nb_stock);
	public List<Optional<Pharmacie>> filterPharmacieByMedicament(Long id_med);
	public boolean isMedicamentDispo(Long id_med,Long id_ph );
}
