package esprit.services;

import java.util.List;
import java.util.Optional;
import esprit.entities.Pharmacie;

public interface PharmacieService {
	public Pharmacie addPharmacie(Pharmacie pharmacie);
	public Optional<Pharmacie> getPharmacie(Long id);
	public List<Pharmacie> getAllPharmacie();
}
