package esprit.entities;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medicament")
public class Medicament implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(unique = true)
	private String name;
	private TypeMedicament type;
	private int nbUnite;
	private String unite;
	private float prix;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TypeMedicament getType() {
		return type;
	}
	public void setType(TypeMedicament type) {
		this.type = type;
	}
	public int getNbUnite() {
		return nbUnite;
	}
	public void setNbUnite(int nbUnite) {
		this.nbUnite = nbUnite;
	}
	public String getUnite() {
		return unite;
	}
	public void setUnite(String unite) {
		this.unite = unite;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
	public Medicament() {
		super();
	}
	public Medicament(String name, TypeMedicament type, int nbUnite, String unite, float prix) {
		super();
		this.name = name;
		this.type = type;
		this.nbUnite = nbUnite;
		this.unite = unite;
		this.prix = prix;
	}
	public Medicament(long id, String name, TypeMedicament type, int nbUnite, String unite, float prix) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.nbUnite = nbUnite;
		this.unite = unite;
		this.prix = prix;
	}

	
	

}
