package esprit.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "gpscoordinate")
public class GpsCoordinate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public GpsCoordinate(Long id, float lat, float lng) {
		this.id = id;
		this.lat = lat;
		this.lng = lng;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private float lat;
	private float lng;
	public GpsCoordinate(float lat, float lng) {
		super();
		this.lat = lat;
		this.lng = lng;
	}
	public GpsCoordinate() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLng() {
		return lng;
	}
	public void setLng(float lng) {
		this.lng = lng;
	}

}
