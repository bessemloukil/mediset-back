package esprit.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery (name = "Stock.findByPharmacie", query = "SELECT p FROM Stock p  where p.pharmacie_id= :pharmacie_id")
@NamedQuery (name = "Stock.filterPharmacieByMedicament", query = "SELECT p FROM Stock p  where p.medicament_id= :medicament_id")
@Table(name = "stock")
public class Stock implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long medicament_id;
	private Medicament medicament;
	private Long pharmacie_id;
	private float quantity;
	public Stock(Long id, Long medicament_id, Medicament medicament, Long pharmacie_id, float quantity) {
		super();
		this.id = id;
		this.medicament_id = medicament_id;
		this.medicament = medicament;
		this.pharmacie_id = pharmacie_id;
		this.quantity = quantity;
	}
	public Stock(Long medicament_id, Medicament medicament, Long pharmacie_id, float quantity) {
		super();
		this.medicament_id = medicament_id;
		this.medicament = medicament;
		this.pharmacie_id = pharmacie_id;
		this.quantity = quantity;
	}
	public Stock() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMedicament_id() {
		return medicament_id;
	}
	public void setMedicament_id(Long medicament_id) {
		this.medicament_id = medicament_id;
	}
	public Medicament getMedicament() {
		return medicament;
	}
	public void setMedicament(Medicament medicament) {
		this.medicament = medicament;
	}
	public Long getPharmacie_id() {
		return pharmacie_id;
	}
	public void setPharmacie_id(Long pharmacie_id) {
		this.pharmacie_id = pharmacie_id;
	}
	public float getQuantity() {
		return quantity;
	}
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	
	
}
