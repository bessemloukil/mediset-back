package esprit.entities;

public enum TypeMedicament {
	COMPRIME,
	POMMADE, 
	SOLUTION,
	GRANNULES, 
	POUDRE,
	GEL, 
	GAZ, 
	BATTON; 

}
