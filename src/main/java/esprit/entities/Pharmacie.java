package esprit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name = "pharmacie")
public class Pharmacie extends User implements Serializable {
/**
	 * 
	 */
private static final long serialVersionUID = 1L;
private boolean night;
private Date nextGuardDate;
private int numberWeekToGuard;
/*@OneToOne(fetch = FetchType.LAZY,
cascade =  CascadeType.ALL,
mappedBy = "gpscoordinate")*/
private GpsCoordinate adresse_id;
public Pharmacie(long id, String userName, String phoneNumber, String password, String email, boolean night,
		Date nextGuardDate, int numberWeekToGuard, GpsCoordinate adresse_id) {
	super(id, userName, phoneNumber, password, email);
	this.night = night;
	this.nextGuardDate = nextGuardDate;
	this.numberWeekToGuard = numberWeekToGuard;
	this.adresse_id = adresse_id;
}
public Pharmacie(String userName, String phoneNumber, String password, String email, boolean night,
		Date nextGuardDate, int numberWeekToGuard, GpsCoordinate adresse_id) {
	super(userName, phoneNumber, password, email);
	this.night = night;
	this.nextGuardDate = nextGuardDate;
	this.numberWeekToGuard = numberWeekToGuard;
	this.adresse_id = adresse_id;
}
public Pharmacie(String userName, String phoneNumber, String email, boolean night,
		Date nextGuardDate, int numberWeekToGuard, GpsCoordinate adresse_id) {
	super(userName, phoneNumber, email);
	this.night = night;
	this.nextGuardDate = nextGuardDate;
	this.numberWeekToGuard = numberWeekToGuard;
	this.adresse_id = adresse_id;
}
public Pharmacie(long id, String userName, String phoneNumber, String password, String email) {
	super(id, userName, phoneNumber, password, email);
}
public Pharmacie() {
	super();
}
public boolean isNight() {
	return night;
}
public void setNight(boolean night) {
	this.night = night;
}
public Date getNextGuardDate() {
	return nextGuardDate;
}
public void setNextGuardDate(Date nextGuardDate) {
	this.nextGuardDate = nextGuardDate;
}
public int getNumberWeekToGuard() {
	return numberWeekToGuard;
}
public void setNumberWeekToGuard(int numberWeekToGuard) {
	this.numberWeekToGuard = numberWeekToGuard;
}
public GpsCoordinate getAdresse_id() {
	return adresse_id;
}
public void setAdresse_id(GpsCoordinate adresse_id) {
	this.adresse_id = adresse_id;
}
@Override
public String toString() {
	return "Pharmacie [night=" + night + ", nextGuardDate=" + nextGuardDate + ", numberWeekToGuard=" + numberWeekToGuard
			+ ", adresse_id=" + adresse_id + "]";
}
}
