package esprit.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "client")
public class Client extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date birthday;


	public Client(String userName, String phoneNumber, String email, Date birthday) {
		super(userName, phoneNumber, email);
		this.birthday = birthday;
	}
	public Client(String userName, String phoneNumber, String email, String password, Date birthday) {
		super(userName, phoneNumber, email,password);
		this.birthday = birthday;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Client(Long id, String userName, String phoneNumber, String password, String email) {
		super(id, userName, phoneNumber, password, email);
	}
	
	public Client() {
		super();
	}
	

	
}
