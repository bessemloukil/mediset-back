package esprit.entities;

import java.util.Date;

import javax.persistence.NamedNativeQueries;
import javax.persistence.SqlResultSetMapping;


public class UserDB {
	private Long id;
	private String userName;
	private String phoneNumber;
	private String password;
	private String email;
	private Date birthday;
	private boolean night;
	private Date nextGuardDate;
	private int numberWeekToGuard;
	private GpsCoordinate adresse_id;
	private String dtype;
	public UserDB(Long id, String userName, String phoneNumber, String password, String email, Date birthday,
			boolean night, Date nextGuardDate, int numberWeekToGuard, String dtype) {
		super();
		this.id = id;
		this.userName = userName;
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.email = email;
		this.birthday = birthday;
		this.night = night;
		this.nextGuardDate = nextGuardDate;
		this.numberWeekToGuard = numberWeekToGuard;
		this.dtype=dtype;
	}
	public GpsCoordinate getAdresse_id() {
		return adresse_id;
	}
	public void setAdresse_id(GpsCoordinate adresse_id) {
		this.adresse_id = adresse_id;
	}
	public String getDtype() {
		return dtype;
	}
	public void setDtype(String dtype) {
		this.dtype = dtype;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public boolean isNight() {
		return night;
	}
	public void setNight(boolean night) {
		this.night = night;
	}
	public Date getNextGuardDate() {
		return nextGuardDate;
	}
	public void setNextGuardDate(Date nextGuardDate) {
		this.nextGuardDate = nextGuardDate;
	}
	public int getNumberWeekToGuard() {
		return numberWeekToGuard;
	}
	public void setNumberWeekToGuard(int numberWeekToGuard) {
		this.numberWeekToGuard = numberWeekToGuard;
	}
	public GpsCoordinate getAdresse() {
		return adresse_id;
	}
	public void setAdresse(GpsCoordinate adresse) {
		this.adresse_id = adresse;
	}
	
}
