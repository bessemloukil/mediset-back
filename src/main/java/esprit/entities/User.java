package esprit.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity
@NamedQuery (name = "User.findByEmail", query = "SELECT p FROM User p  where p.email= :email")
@NamedQuery (name = "User.AddAdresse", query = "UPDATE User p SET p.adresse_id= :adresse_id WHERE p.userName= :user_name")
@Table(name = "user")



public abstract class User  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String userName;
	private String phoneNumber;
	private String password;
	@Column(unique = true)
	private String email;
	public User(Long id, String userName, String phoneNumber, String password, String email) {
		super();
		this.id = id;
		this.userName = userName;
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.email = email;
	}
	public User(String userName, String phoneNumber, String email) {
		super();
		this.userName = userName;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}
	public User(String userName, String phoneNumber, String password, String email) {
		super();
		this.userName = userName;
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.email = email;
	}
	public User() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	
}
