package esprit.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "don")
public class Don  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long client_id;
	private Stock[] stock;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Long getClient() {
		return client_id;
	}
	public void setClient(Long client_id) {
		this.client_id = client_id;
	}
	public Stock[] getStock() {
		return stock;
	}
	public void setStock(Stock[] stock) {
		this.stock = stock;
	}
	public Don() {
		super();
	}
	public Don(Long client_id, Stock[] stock) {
		super();
		this.client_id = client_id;
		this.stock = stock;
	}
	public Don(long id, Long client_id, Stock[] stock) {
		super();
		this.id = id;
		this.client_id = client_id;
		this.stock = stock;
	}
}
