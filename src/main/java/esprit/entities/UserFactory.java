package esprit.entities;

import esprit.services.Factory;
import esprit.servicesImpl.ClientFactory;
import esprit.servicesImpl.PharmacieFactory;

public class UserFactory implements Factory<User, UserDB> {

	public static final int client = 1;
	public static final int pharmacie = 2;

	private Factory<User, UserDB> getFactory(UserDB userDB) {
		int typeUser = 0;
		if (userDB.getDtype().equals("Client")||userDB.getDtype().equals("client") ) {
			typeUser = 1;
		} else if (userDB.getDtype().equals("Pharmacie")||userDB.getDtype().equals("pharmacie") ) {
			typeUser = 2;
		}
		switch (typeUser) {
		case client:
			return new ClientFactory();
		case pharmacie:
			return new PharmacieFactory();
		default:
			throw new IllegalArgumentException("Type de user inconnu!!!");
		}
	}

	@Override
	public User create(UserDB userDB) {
		Factory<User, UserDB> f = getFactory(userDB);
		return f.create(userDB);
	}
}
