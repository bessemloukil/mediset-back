package esprit.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery (name = "Report.getReportsByPharmacie", query = "SELECT p FROM Report p  where p.pharmacie_id = :pharmacie_id")
@Table(name = "report")
public class Report implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int pharmacie_id;
	private Client client;
	private Medicament medicament;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPharmacie_id() {
		return pharmacie_id;
	}

	public void setPharmacie_id(int pharmacie_id) {
		this.pharmacie_id = pharmacie_id;
	}

	
	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Medicament getMedicament() {
		return medicament;
	}

	public void setMedicament(Medicament medicament) {
		this.medicament = medicament;
	}

	public Report() {
		super();
	}

	public Report(Object object, int pharmacie_id, Client client, Medicament medicament) {
		super();
		this.pharmacie_id = pharmacie_id;
		this.client = client;
		this.medicament = medicament;
	}

	
	
}
