package esprit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "esprit.dao")
@EntityScan("esprit.entities")
@ComponentScan


public class MediSettApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediSettApplication.class, args);
		System.out.println("zz");
	}

}
