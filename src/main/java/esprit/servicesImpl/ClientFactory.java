package esprit.servicesImpl;

import esprit.entities.Client;
import esprit.entities.User;
import esprit.entities.UserDB;
import esprit.services.Factory;

public class ClientFactory implements Factory<User, UserDB> {

	@Override
	public Client create(UserDB userDB) {
		// TODO Auto-generated method stub
		return  new Client(userDB.getUserName(),userDB.getPhoneNumber(),userDB.getPassword(),userDB.getEmail(),
  			  userDB.getBirthday());
	}

}
