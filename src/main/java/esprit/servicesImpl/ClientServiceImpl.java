package esprit.servicesImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import esprit.dao.ClientRepository;
import esprit.entities.Client;
import esprit.services.ClientService;

@RestController
public class ClientServiceImpl implements ClientService {
	@Autowired
	ClientRepository clientRepository;
	
	@RequestMapping(value = "mediset/addClient/{password}",consumes = "application/json", produces = "application/json")
	public Client addClient(@RequestBody Client client, @PathVariable(value="password") String password ) {
		client.setPassword(password);
		return clientRepository.save(client);
	
	}
}
