package esprit.servicesImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import esprit.dao.UserRepository;
import esprit.entities.GpsCoordinate;
import esprit.entities.User;
import esprit.entities.UserDB;
import esprit.entities.UserFactory;
import esprit.services.Factory;
import esprit.services.UserService;




@RestController
public class UserServiceImpl implements UserService{
	@Autowired
	UserRepository userRepository;
	Connection connection = utils.DataSource.getInstance().getConnection();
    PreparedStatement preparedStatement;

	@RequestMapping(value = "mediset/login/{userName}/{password}", method = RequestMethod.GET)
	public User login(@PathVariable(value="userName") String userName,@PathVariable(value="password") String password) {
		/*User u=  userRepository.findByEmail(email);
		if (u.getPassword().equals(password)) {
			return u;
		}
		else return null;
	}*/  UserDB userDb;
		try {
		 PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM User where user_name= ? AND password= ?");
		 preparedStatement.setString(1, userName);
		 preparedStatement.setString(2, password);
		 ResultSet resultSet = preparedStatement.executeQuery();
         while (resultSet.next()) {
        	  userDb = new UserDB(resultSet.getLong("id"),
        			 resultSet.getString("user_name"),
        			 resultSet.getString("phone_number"),
        			 resultSet.getString("password"),
        			 resultSet.getString("email"),
        			 resultSet.getDate("birthday"),
        			 resultSet.getBoolean("night"),
        			 resultSet.getDate("next_guard_date"),
        			 resultSet.getInt("number_week_to_guard"),
        			 resultSet.getString("dtype"));
        	  		// userDb.setAdresse((GpsCoordinate) resultSet.getBlob("adresse_id"));
        	  		 Factory<User,UserDB> uf = new UserFactory();
        	  		User u =  uf.create(userDb);
        	  		return u;
     
         }
         
		}
		 catch(Exception e) {
				 System.out.println(e.getMessage().toString());
				 return null;
			 
		 }
		return null;


}
}

