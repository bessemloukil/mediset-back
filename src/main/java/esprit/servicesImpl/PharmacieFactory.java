package esprit.servicesImpl;

import esprit.entities.Pharmacie;
import esprit.entities.User;
import esprit.entities.UserDB;
import esprit.services.Factory;

public class PharmacieFactory implements Factory<User, UserDB> {

	@Override
	public Pharmacie create(UserDB userDB) {
		// TODO Auto-generated method stub
		return new Pharmacie(userDB.getId(), userDB.getUserName(), userDB.getPhoneNumber(), userDB.getEmail(),userDB.getPassword(),
  			  userDB.isNight(), userDB.getNextGuardDate(),userDB.getNumberWeekToGuard(), userDB.getAdresse());
	}

}
