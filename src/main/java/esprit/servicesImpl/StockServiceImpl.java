package esprit.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import esprit.dao.PharmacieRepository;
import esprit.dao.StockRepository;
import esprit.entities.Medicament;
import esprit.entities.Pharmacie;
import esprit.entities.Stock;
import esprit.services.StockService;

@RestController
public class StockServiceImpl implements StockService {
	@Autowired
	PharmacieRepository pharmacieRepository;
	@Autowired
	StockRepository stockRepository;
	
	@RequestMapping(value = "mediset/stockPharmacie/{id}", method = RequestMethod.GET)
	public List<Stock> getStocks(@PathVariable(value="id") Long id) {

		return stockRepository.findByPharmacie(id);
	}
	
	@RequestMapping(value = "mediset/addStock/{id_ph}/{nb_stock}",consumes = "application/json", produces = "application/json")
	public List<Stock> addStock(@PathVariable(value="id_ph") Long id_ph,@PathVariable(value="nb_stock") float nb_stock, @RequestBody Medicament medicament) {
		
		Stock s = new  Stock( medicament.getId(), medicament,id_ph, nb_stock);
		stockRepository.save(s);
		return stockRepository.findByPharmacie(id_ph);
	}
	
	@RequestMapping(value = "mediset/updateStock/{id_stock}/{value}", method = RequestMethod.PUT)
	public boolean updateStock(@PathVariable(value="id_stock") Long id_stock,@PathVariable(value="value") float value) {
		Optional<Stock> s = stockRepository.findById(id_stock);
		Stock stock = s.get();
		stock.setQuantity(value);
		stockRepository.save(stock);
		return true;
	}
	
	@RequestMapping(value = "mediset/filterPharmacieByMedicament/{id_med}", method = RequestMethod.GET)
	public List<Optional<Pharmacie>> filterPharmacieByMedicament(@PathVariable(value="id_med") Long id_med ) {
		List<Stock> StocksPharmacies = stockRepository.filterPharmacieByMedicament(id_med);
		List<Optional<Pharmacie>> pharmacies = new ArrayList<Optional<Pharmacie>>();
		for (Stock s : StocksPharmacies) {
			pharmacies.add(pharmacieRepository.findById(s.getPharmacie_id())) ;
		}
		return pharmacies;
	}
	
	@RequestMapping(value = "mediset/isMedicamentDispo/{id_med}/{id_ph}", method = RequestMethod.GET)
	public boolean isMedicamentDispo(@PathVariable(value="id_med") Long id_med, @PathVariable(value="id_ph") Long id_ph ) {
		List<Optional<Pharmacie>> pharmacies = filterPharmacieByMedicament(id_med ); 
		List l = pharmacies.stream().filter(p-> p.get().getId()==id_ph).collect(Collectors.toList());
		if (l.isEmpty()) {
			return false;
		}
		else return true;
	}
	
}
