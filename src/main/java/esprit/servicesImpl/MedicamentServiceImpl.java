package esprit.servicesImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import esprit.dao.MedicamentRepository;
import esprit.entities.Medicament;


@RestController
public class MedicamentServiceImpl {
	@Autowired
	MedicamentRepository medicamentRepository;
	
	@RequestMapping(value = "mediset/search/{input}", method = RequestMethod.GET)
	public Object[] search(@PathVariable(value="input") String input) {
		return medicamentRepository.findAll().stream().filter(medicament -> medicament.getName().contains(input)).toArray();
	}
	
	@RequestMapping(value = "mediset/allMedicament", method = RequestMethod.GET)
	public List<Medicament> all() {
		return medicamentRepository.findAll();
	}
	
}
