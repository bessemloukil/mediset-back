package esprit.servicesImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import esprit.dao.ClientRepository;
import esprit.dao.MedicamentRepository;
import esprit.dao.ReportRepository;
import esprit.entities.Client;
import esprit.entities.Medicament;
import esprit.entities.Report;
import esprit.services.ReportService;


@RestController
public class ReportServiceImpl implements ReportService {
	@Autowired
	ReportRepository ReportRepository;
	@Autowired
	ClientRepository clientRepository;
	@Autowired
	MedicamentRepository MedicamentRepository;
	
	@RequestMapping(value = "mediset/getReportsByPharmacie/{id}", method = RequestMethod.GET)
	public List<Report> getReportsByPharmacie(@PathVariable(value="id") int id) {
		return ReportRepository.getReportsByPharmacie(id);
	}
	
	@RequestMapping(value = "mediset/report/{pharmacie_id}/{client_id}/{medicament_id}", method = RequestMethod.GET)
	public Boolean addReport(@PathVariable(value="pharmacie_id") int pharmacie_id
			, @PathVariable(value="client_id") int client_id
			, @PathVariable(value="medicament_id") int medicament_id) {
		
		Client client = new Client();
		client = clientRepository.findById((long) client_id).get();
		Medicament medicament = new Medicament();
		medicament = MedicamentRepository.findById((long) medicament_id).get();

		Report report = new Report(null,pharmacie_id, client, medicament);
		ReportRepository.save(report);
		
		return true;
	}
}
