package esprit.servicesImpl;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException.Gone;

import esprit.dao.GpsCoordinateRepository;
import esprit.dao.PharmacieRepository;
import esprit.dao.UserRepository;
import esprit.entities.GpsCoordinate;
import esprit.entities.Pharmacie;
import esprit.entities.User;
import esprit.entities.UserDB;
import esprit.entities.UserFactory;
import esprit.services.Factory;
import esprit.services.PharmacieService;


@RestController
public class PharmacieServiceImpl{
	@Autowired
	PharmacieRepository pharmacieRepository;
	@Autowired
	GpsCoordinateRepository gpsCoordinateRepository;
	@Autowired
	UserRepository userRepository;
	Connection connection = utils.DataSource.getInstance().getConnection();
    PreparedStatement preparedStatement;

	/*@RequestMapping(value = "mediset/addPharmacie",consumes = "application/json", produces = "application/json")
	public Pharmacie addPharmacie(@RequestBody Pharmacie pharmacie) {
		/*System.out.println(pharmacie.toString());
		pharmacie.setAdresse_id(gpsCoordinateRepository.save(pharmacie.getAdresse_id()));
		pharmacieRepository.save(pharmacie);

		
		try {
		 PreparedStatement preparedStatement = connection.prepareStatement("UPDATE user p SET p.adresse_id= ? WHERE p.user_name= ?");
		 preparedStatement.setLong(1, pharmacie.getAdresse_id().getId());
		 preparedStatement.setString(2, pharmacie.getUserName());
		 preparedStatement.execute();
                
		}
		 catch(Exception e) {
				 System.out.println(e.getMessage().toString());
				 return null;
			 
		 }
		return pharmacie;
	
	}*/
    @RequestMapping(value = "mediset/addPharmacie/{password}",consumes = "application/json", produces = "application/json")
	public Pharmacie addPharmacie(@RequestBody Pharmacie pharmacie, @PathVariable String password) {
    	pharmacie.setPassword(password);
    	pharmacie.setAdresse_id(gpsCoordinateRepository.save(pharmacie.getAdresse_id()));
    	return pharmacieRepository.save(pharmacie);
    }
	
	@RequestMapping(value = "mediset/getPharmacie/{id}", method = RequestMethod.GET)
	public Optional<Pharmacie> getPharmacie(@PathVariable(value="id") Long id) {
		return pharmacieRepository.findById(id);
	}
	
	@RequestMapping(value = "mediset/getAllPharmacie", method = RequestMethod.GET)
	public List<Pharmacie> getAllPharmacie() {
		return pharmacieRepository.findAll();
	}
}
